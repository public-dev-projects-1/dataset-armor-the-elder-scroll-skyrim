# Dataset Armor - The Elder Scroll - Skyrim



I created this project to show how to obtain a table from a website, process the data and then send it to Kaggle.

- [Kaggle Dataset](https://www.kaggle.com/datasets/sc0v1n0/the-elder-scroll-skyrim-armor)
- [The Elder Scrolls Skyrim Wiki](https://elderscrolls.fandom.com/wiki/Armor_(Skyrim))
- [Creating Dataset The Elder Scroll: Skyrim Armor and Sending to Kaggle Datasets](https://dev.to/sc0v0ne/create-dataset-preprocessing-and-add-kaggle-datasets-3n0o-temp-slug-1155139?preview=dc9bf4962611d4d00accd053f3de5de5b72507dfdb3d7a399b034fe39549525c3b076cb9e63ccb1bb5ac9951b96af14abe8b30b1d1f9d3f9ebf0cfb5)
